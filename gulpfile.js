// generated on 2016-09-10 using generator-chrome-extension 0.6.0
var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var del = require('del');
var runSequence = require('run-sequence');
var wiredep = require('wiredep').stream;

const $ = gulpLoadPlugins();
var Linter = require("tslint");
var tslintProgram = Linter.createProgram("tsconfig.json", "app/scripts.ts/");
var tsProject = $.typescript.createProject("tsconfig.json");
var sourceTsFiles = ['typings/index.d.ts', 'app/scripts.ts/**/*.ts']

gulp.task('lint', function () {
  console.log(Linter.getFileNames(tslintProgram));
  return gulp.src(sourceTsFiles)
    .pipe($.tslint({
      formatter: "verbose",
      program: tslintProgram
    }))
    .pipe($.tslint.report({
      emitError: false
    }));
});

gulp.task('typings', function(){
  return gulp.src("./typings.json").pipe($.typings());
});

gulp.task('ts', ['clean'], function () {
  var tsResult = gulp.src(sourceTsFiles)
    .pipe($.sourcemaps.init())
    .pipe($.typescript(tsProject));

  tsResult.dts.pipe(gulp.dest('app/scripts'));

  return tsResult.js
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('app/scripts'));

  // .on('error', function(err) {
  //   console.log('error Typescript: ' + err.message + '\n' + err.codeFrame);
  //   this.end();
  // })

  // var dist = 'app/scripts';
  // return Promise.all([
  //   new Promise(function(resolve, reject) {
  //     tsResult.dts.pipe(gulp.dest('app/scripts'))
  //       .on('error', reject)
  //       .pipe(gulp.dest(dist))
  //       .on('end', resolve)
  //   }),
  //   new Promise(function(resolve, reject) {
  //     tsResult.js.pipe(gulp.dest('app/scripts'))
  //       .on('error', reject)
  //       .pipe(gulp.dest(dist))
  //       .on('end', resolve)
  //   })
  // ])
});

gulp.task('extras', function () {
  return gulp.src([
    'app/*.*',
    'app/_locales/**',
    '!app/scripts.ts',
    '!app/*.json',
    '!app/*.html',
  ], {
    base: 'app',
    dot: true
  }).pipe(gulp.dest('dist'));
});

gulp.task('images', function () {
  return gulp.src('app/images/**/*')
    .pipe($.if($.if.isFile, $.cache($.imagemin({
      progressive: true,
      interlaced: true,
      // don't remove IDs from SVGs, they are often used
      // as hooks for embedding and styling
      svgoPlugins: [{cleanupIDs: false}]
    }))
    .on('error', function(err) {
      console.log(err);
      this.end();
    })))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('html', function () {
  return gulp.src('app/*.html')
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe($.sourcemaps.init())
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.cleanCss({compatibility: '*'})))
    .pipe($.sourcemaps.write())
    .pipe($.if('*.html', $.htmlmin({removeComments: true, collapseWhitespace: true})))
    .pipe(gulp.dest('dist'));
});

gulp.task('chromeManifest', function () {
  return gulp.src('app/manifest.json')
    .pipe($.chromeManifest({
      buildnumber: true,
      background: {
        target: 'scripts/background.js',
        exclude: [
          'scripts/chromereload.js'
        ]
      }
  }))
  .pipe($.if('*.css', $.cleanCss({compatibility: '*'})))
  .pipe($.if('*.js', $.sourcemaps.init()))
  .pipe($.if('*.js', $.uglify()))
  .pipe($.if('*.js', $.sourcemaps.write('.')))
  .pipe(gulp.dest('dist'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist', 'app/scripts']));

gulp.task('watch',  ['lint', 'ts', 'wiredep'],function () {
  $.livereload.listen();

  gulp.watch([
    'app/*.html',
    'app/scripts/**/*.js',
    'app/images/**/*',
    'app/styles/**/*',
    'app/_locales/**/*.json'
  ]).on('change', $.livereload.reload);

  gulp.watch('app/scripts.ts/**/*.js', ['lint', 'ts']);
  gulp.watch('bower.json', ['wiredep']);
});

gulp.task('size', function () {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('wiredep', function () {
  gulp.src('app/*.html')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)*\.\./
    }))
    .pipe(gulp.dest('app'));
});

gulp.task('package', function () {
  var manifest = require('./dist/manifest.json');
  return gulp.src('dist/**')
      .pipe($.zip('anki-translator-' + manifest.version + '.zip'))
      .pipe(gulp.dest('package'));
});

gulp.task('build', (cb) => {
  runSequence(
    'lint', 'ts', 'chromeManifest',
    ['wiredep', 'html', 'images', 'extras'],
    'size', cb);
});

gulp.task('default', ['clean'], cb => {
  runSequence('build', cb);
});
