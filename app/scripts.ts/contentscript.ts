class Dialog {
  constructor() {
    this.str = "asd"
    console.log("Dialog constructor")
    const url = chrome.extension.getURL("templates/dialog.html");
    $.ajax(url).then((data) => {
      this.html = $.parseHTML(data);
      $("body").append(this.html);
    });

    window.addEventListener("dblclick", (event) => this.show(event));
    // window.addEventListener("click", this.hide);
  }
  show(event) {
    console.log("show", this.str);
    let selection = document.getSelection().toString();
    this.html.style.right = event.clientX;
    this.html.style.top = event.clientY;
    this.html.animate({ opacity: "toggle", height: "toggle" }, "fast");
  }

  hide(event) {
    console.log("hide", event);
    this.html.animate({ opacity: "toggle", height: "toggle" }, "fast");
  }
}

// $( ()=>{
  const dialog = new Dialog();
  console.log(dialog);
// })
// window.addEventListener("click",removePopup);

// chrome.runtime.onMessage.addListener(
//   function(request, sender, sendResponse) {
//     switch (request.message) {
//       case "clicked_browser_action":
//         console.log("clicked_browser_action");
//         break;
//       default:
//         console.log("sorry");
//     }
//   }
// );